#creating deepcopy of model instances
from copy import deepcopy

#array manipulations
import numpy as np

#dataframe and its manipulations
import pandas as pd

#plotting
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns

#selected plotting functions
# from statsmodels.graphics.tsaplots import plot_acf,plot_pacf

#classes for grid search and cross-validation, function for splitting data and evaluating models
from sklearn.model_selection import GridSearchCV,RandomizedSearchCV,train_test_split
from skopt import BayesSearchCV


from sklearn.metrics import accuracy_score,f1_score,roc_auc_score,confusion_matrix,roc_curve

#XGBoost library
import xgboost as xgb
import glob
#Python standard libraries
import time
import warnings

#setting default resolution of figures
mpl.rcParams['figure.dpi'] = 200

# import ROOT

from sklearn.model_selection import PredefinedSplit, GridSearchCV

import uproot
train_variable = ['p_Mjl01','min_DR_tau_lep','p_DR_close_jet_lead_led','min_M_tau_lep','p_DRll01','p_Mjl00','p_Mjjl011']



# train_variable = ['p_Mjl01','min_M_tau_lep','p_DRll01','p_Mjl00','p_Mjjl011']

pwd = '/scratch/users/sbataju/2llSS1tauOS/Med/CV'
all_file = {file_.split('/')[-1]:uproot.open(file_+':nominal').arrays(train_variable,library="np")  for file_ in list(set(glob.glob('/scratch/users/sbataju/2llSS1tauOS/merge/Med_*.root'))- set(glob.glob('/scratch/users/sbataju/2llSS1tauOS/merge/Med_sig*.root'))- set(glob.glob('/scratch/users/sbataju/2llSS1tauOS/merge/Med_DYMLowZ.root'))- set(glob.glob('/scratch/users/sbataju/2llSS1tauOS/merge/Med_VV.root')))}
print(all_file)
INPUT_SIG_FILE = uproot.open('/scratch/users/sbataju/2llSS1tauOS/merge/Med_Signal_.root'+':nominal').arrays(train_variable,library="np")
INPUT_BKG_FILE = uproot.open('/scratch/users/sbataju/2llSS1tauOS/merge/Med_VV.root'+':nominal').arrays(train_variable,library="np")
signal_file =uproot.create(pwd+'/out/Med_signal.root')
bkg_file =uproot.create(pwd+'/out/Med_VV.root')
up_files = { file_.split('/')[-1]:uproot.create(pwd+'/out/'+file_.split('/')[-1]) for file_ in all_file.keys() }
df = pd.read_csv('train_test.csv')
y_df = df.pop('class')
#train test split with randomization performed (althoughsplit_indexdomization is not necessary)
X_train, X_test, y_train, y_test = train_test_split(df[train_variable],y_df, test_size=0.25)

from sklearn.metrics import plot_roc_curve

y_df = df.pop('class')

test_fold = [1]*int(len(df)/4)+[2]*int(len(df)/4)+[3]*int(len(df)/4) + [0]*int(len(df)/4)
# print(split_index)
ps = PredefinedSplit(test_fold)
print(ps.get_n_splits())

bkg_fold = {}
for key in all_file.keys():
    df1 = all_file[key][train_variable[0]]
    bfold =  [1]*int(len(df)/4)+[2]*int(len(df)/4)+[3]*int(len(df)/4) + [0]*int(len(df)/4)
    bkg_fold[key] = PredefinedSplit(bfold)
print(bkg_fold,f'  {key}')
    
X = np.concatenate((X_train,X_test),axis=0)
y = np.concatenate((y_train,y_test),axis=0)

print(ps)
results_dict = {}
X = np.concatenate((X_train,X_test),axis=0)
y = np.concatenate((y_train,y_test),axis=0)
c = 0
def CreateBalancedSampleWeights(y_train, largest_class_weight_coef):
    classes = np.unique(y_train, axis = 0)
    classes.sort()
    class_samples = np.bincount(y_train)
    total_samples = class_samples.sum()
    n_classes = len(class_samples)
    weights = total_samples / (n_classes * class_samples * 1.0)
    class_weight_dict = {key : value for (key, value) in zip(classes, weights)}
    class_weight_dict[classes[1]] = class_weight_dict[classes[1]] * largest_class_weight_coef
    sample_weights = [class_weight_dict[y] for y in y_train]
    return sample_weights

                                                                                                #OrderedDict([('gamma', 1.6), ('learning_rate', 0.03), ('max_depth', 12), ('n_estimators', 150), ('reg_alpha', 0.8), ('reg_lambda', 0.2)])}
##'best_params': OrderedDict([('gamma', 1.6), ('learning_rate', 0.6), ('max_depth', 10), ('n_estimators', 65), ('reg_alpha', 3.2), ('reg_lambda', 12.8)])}
bkg_data={}
for train_index, test_index in ps.split():
    c +=1
    print("TRAIN:", train_index, "TEST:", test_index)
    X_train, X_test = X[train_index], X[test_index]
    y_train, y_test = y[train_index], y[test_index]
    largest_class_weight_coef = max(pd.Series(y).value_counts().values)/df.shape[0]
    weight = CreateBalancedSampleWeights(y_train, largest_class_weight_coef)
    #XGBClassifier default parameters are not available without fitting the model
    # xgbc0 = cv_result['xgbc_bcv']['best_estimator']                                                                          # 'best_params': OrderedDict([('gamma', 3.2),('learning_rate', 0.06),('max_depth', 7),('n_estimators', 80),('reg_alpha', 0.0),('reg_lambda', 6.4)])}}
    xgbc0 = xgb.XGBClassifier(objective='binary:logistic', booster='gbtree', eval_metric='logloss', use_label_encoder=False,colsample_bytree=0.8 ,gamma=1.6,learning_rate=0.15,max_depth=10,n_estimators=130,reg_alpha=0.4,reg_lambda=102.4,subsample=0.6)
                                                                                           # best_params': OrderedDict([('gamma', 1.6), ('learning_rate', 0.06), ('max_depth', 12), ('n_estimators', 100), ('reg_alpha', 0.8), ('reg_lambda', 3.2)])}
                                                                                #OrderedDict([('gamma', 0.1), ('learning_rate', 0.03), ('max_depth', 8), ('n_estimators', 300), ('reg_alpha', 0.2), ('reg_lambda', 6.4)])
                                                                                #OrderedDict([('colsample_bytree', 0.6), ('gamma', 0.2), ('learning_rate', 0.1), ('max_depth', 7), ('n_estimators', 100), ('reg_alpha', 1.6), ('reg_lambda', 0.4), ('subsample', 0.6)])}
                                                                                # OrderedDict([('colsample_bytree', 0.8), ('gamma', 1.6), ('learning_rate', 0.15), ('max_depth', 10), ('n_estimators', 130), ('reg_alpha', 0.4), ('reg_lambda', 102.4), ('subsample', 0.6)])

    xgbc0.fit(X_train,y_train,sample_weight=weight)
    results_dict[f'clf{c}_train_prob'] = xgbc0.predict_proba(X_train)
    results_dict[f'clf{c}_test_prob'] = xgbc0.predict_proba(X_test)
    results_dict[f'y_test{c}'] = y_test
    results_dict[f'y_train{c}'] = y_train
    results_dict[f'clf{c}'] = xgbc0
    results_dict[f'X_train{c}'] = X_train
    results_dict[f'X_test{c}'] = X_test
    
    for key in all_file.keys():
        # print(all_file[key])
        # print(bkg_fold[key])
        # print(bkg_fold[key].get_n_splits())
        count=0
        df1 = pd.DataFrame.from_dict(all_file[key])
        # print(df1)
        for train_key,test_key in bkg_fold[key].split():
            count+=1
            print("TRAIN key:", train_key, "TEST key:", test_key, key)
            # print(0,key,count,train_key,test_key)
            if c!=count: continue
            # print(888,key,count,train_key,test_key)  
            # print(df1.iloc[test_key])
            # print(xgbc0.predict_proba(df1.iloc[test_key]))
            bkg_data[f'{count}'+key]=xgbc0.predict_proba(df1.iloc[test_key])[:,1]
            print(f'{count}'+key)
            # up_files[key].update(pwd+'/out/'+key,)
            # up_files[key].mktree("nominal", {"bdt_out": np.array(xgbc0.predict_proba(df1.iloc[test_key])[:,1])})
                # up_files[key]['nominal'] = np.array(xgbc0.predict_proba(df1.iloc[test_key])[:,1], dtype='float32')
    
    
    
    fig = plt.figure()
    plt.bar(range(len(xgbc0.feature_importances_)), xgbc0.feature_importances_)
    # plt.set_xticks(results_dict['clf1'].feature_importances_)
    # plt.set_xticklabels(train_variable)
    plt.xticks(range(len(xgbc0.feature_importances_)), train_variable)
    # plt.show()
    plt.savefig(f'out/test1_gpu_featureimp_{c}.png')
    plt.close(fig)
    
    
#combine all bkgs    
all_out={}
for key in all_file.keys():
    all_out[key] = np.concatenate([bkg_data[f'{i}'+key] for i in [1,2,3,4,5,6,7] ]) #no of folds
    up_files[key].mktree("nominal", {"bdt_out": np.float64})
    up_files[key]["nominal"].extend({"bdt_out":all_out[key]})
    
    
print(bkg_data)



# assert False
f, axes = plt.subplots(nrows=4,ncols=1,figsize=(9,18))
for i,ax in enumerate(axes):
    i=i+1
    rfc_disp = plot_roc_curve(results_dict['clf{}'.format(i)], results_dict["X_test{}".format(i)], results_dict["y_test{}".format(i)], ax=ax)
# plt.show()
plt.savefig('out/test1_gpu_roc.png')
plt.close(f)

fig  = plt.figure(figsize=(5, 5))
avg_roc = []
avg_fpr = []
avg_tpr = []
tprs = []
base_fpr = np.linspace(0, 1, 101)
for i in [1,2,3,4]:
    roc1 = roc_curve(results_dict['y_test{}'.format(i)], results_dict['clf{}_test_prob'.format(i)][:,1])

    fpr,tpr,_=roc1
    avg_roc.append(roc1)
    avg_fpr.append(fpr)
    avg_tpr.append(tpr)
    plt.plot(fpr, tpr, 'b')
    tpr = np.interp(base_fpr, fpr, tpr)
    tpr[0] = 0.0
    tprs.append(tpr)
tprs = np.array(tprs)
mean_tprs = tprs.mean(axis=0)
std = tprs.std(axis=0)
tprs_upper = np.minimum(mean_tprs + std, 1)
tprs_lower = mean_tprs - std


plt.plot(base_fpr, mean_tprs, 'r')
plt.fill_between(base_fpr, tprs_lower, tprs_upper, color='grey', alpha=0.3)

plt.plot([0, 1], [0, 1],'r--')
plt.xlim([-0.01, 1.01])
plt.ylim([-0.01, 1.01])
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')
plt.savefig('out/test1_gpu_combroc.png')
# plt.show()
plt.close(fig)
# print([len(i) for i in avg_fpr])
# print([len(i) for i in avg_tpr])
# print([len(i) for i in avg_roc])

output_sig = np.concatenate([results_dict['clf{}_test_prob'.format(i)][ results_dict['y_test{}'.format(i)]==1][:,1] for i in [1,2,3,4]],axis=0)
output_sigtrain = np.concatenate([results_dict['clf{}_train_prob'.format(i)][ results_dict['y_train{}'.format(i)]==1][:,1] for i in [1,2,3,4]],axis=0)
output_bkg = np.concatenate([results_dict['clf{}_test_prob'.format(i)][ results_dict['y_test{}'.format(i)]==0][:,1] for i in [1,2,3,4]],axis=0)
output_bkgtrain = np.concatenate([results_dict['clf{}_train_prob'.format(i)][ results_dict['y_train{}'.format(i)]==0][:,1] for i in [1,2,3,4]],axis=0)
fig = plt.figure()
plt.hist(output_sig,color='blue',bins='auto',label='Test, Signal',density=True,alpha=0.4)
plt.hist(output_sigtrain,color='blue',bins='auto',label='Train, Signal',density=True,alpha=0.4,histtype='step')
plt.hist(output_bkg,color='red',bins='auto',label='Test, Bkg',density=True,alpha=0.4)
plt.hist(output_bkgtrain,color='red',bins='auto',label='Train, Bkg',density=True,histtype='step')
# plt.yscale('log')
plt.legend()
# plt.show()

plt.savefig('out/test1_gpu_trainvstest.png')
plt.close(fig)

fig = plt.figure()
plt.hist(output_sig,color='blue',bins='auto',label='Test, Signal',density=True,alpha=0.4)
plt.hist(output_sigtrain,color='blue',bins='auto',label='Train, Signal',density=True,alpha=0.4,histtype='step')
plt.hist(output_bkg,color='red',bins='auto',label='Test, Bkg',density=True,alpha=0.4)
plt.hist(output_bkgtrain,color='red',bins='auto',label='Train, Bkg',density=True,histtype='step')
plt.yscale('log')
plt.legend()
# plt.show()

plt.savefig('out/test1_gpu_trainvstestlog.png')
plt.close(fig)

