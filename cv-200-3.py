#creating deepcopy of model instances
from copy import deepcopy

#array manipulations
import numpy as np

#dataframe and its manipulations
import pandas as pd

#plotting
# import matplotlib 
# matplotlib.use('agg')

import matplotlib as mpl
mpl.use('agg')
import matplotlib.pyplot as plt
import seaborn as sns
#selected plotting functions
# from statsmodels.graphics.tsaplots import plot_acf,plot_pacf

#classes for grid search and cross-validation, function for splitting data and evaluating models
from sklearn.model_selection import train_test_split,StratifiedKFold
from skopt import BayesSearchCV
from skopt.space import Real, Categorical, Integer

from sklearn.metrics import accuracy_score,f1_score,roc_auc_score,confusion_matrix,roc_curve

#XGBoost library
import xgboost as xgb
import glob
#Python standard libraries
import time
import warnings

#setting default resolution of figures
# mpl.rcParams['figure.dpi'] = 200

# import ROOT
from sklearn.inspection import PartialDependenceDisplay
from sklearn.model_selection import PredefinedSplit, GridSearchCV
from sklearn.metrics import plot_roc_curve,auc
import uproot
from skopt.plots import plot_objective, plot_histogram
import optuna
from sklearn.metrics import accuracy_score
from optuna import Trial, visualization

import os

import sklearn

import math


def pDeltaR(eta1,eta2,phi1,phi2):
    Deta = eta1 - eta2
    Dphi = phi1 - phi2
    if( Dphi > PI ): Dphi = Dphi - (2*PI)
    if( Dphi < -PI ): Dphi = Dphi + (2*PI)
    
    Cone = math.sqrt( Deta**2 + Dphi**2)
    
    return Cone


getpDeltaR = np.vectorize(pDeltaR, otypes=[np.float64])


# Only keep some variables for training
# variable_names = ['lep_Pt_0','lep_Pt_1','HT_jets','Mll01','Ptll01','HT_lep','DRll01','lep_Phi_0','lep_Phi_1','lep_Eta_0','lep_Eta_1','tau_pt_0','nJets_OR','tau_eta_0','tau_phi_0','HT','met_met','met_phi','lead_jetPt','lead_jetEta','lead_jetPhi','sublead_jetPt','sublead_jetEta','sublead_jetPhi','angle_leadjet_tau','angle_subleadjet_tau','min_DR_lep_leadjet','min_DR_lep_subleadjet','min_DR_tau_jet','min_DR_tau_lep','min_M_lep_leadjet','min_M_lep_subleadjet','min_M_tau_jet','min_M_tau_lep','p_DRjl01','p_DRjl11','p_DRjl00','p_DRjl10','p_DRtauj00','p_DRtauj01','p_DRtaul01','p_DRtaul00','p_Mjl01','p_Mjl00','p_Mjl10','p_Mjl11','p_Mtaul01','p_Mtaul00','p_Mtauj01','p_Mtauj00','p_DRmetl0','p_DRmetl1','p_DRmettau0','p_DPhill01','p_Mmetl0','p_Mmetl1','p_Mmettau0','p_DR_close_jet_lead_led','p_DR_far_jet_lead_led','sum_pt','p_DPhijl00','p_Mjjl011']
# variable_names = ['p_Mjl01','min_DR_tau_lep','p_DR_close_jet_lead_led','min_M_tau_lep','p_DRll01','p_Mjl00','p_Mjjl011','angle_leadjet_tau','angle_subleadjet_tau']

# variable_names = ['LBoostL1Tau_DRL1J2','LBoostL2Tau_DRL2J1','p_DRll01','p_DRjl00','p_Mjl00','p_Mjl10','p_Mjl01','p_M_closerLepToTau','p_DR_closerJetToLeadLep','p_M_closerJetToLeadLep','p_DR_closerJetToSubLeadLep','p_invMl2j1j2','LBoost2L_AngleTauJ1','LBoost2L_AngleTauJ2','lead_jetPt','min_DR_lep_leadjet','min_M_lep_leadjet']

variable_names = ['LBoostL1Tau_DRL1J2','LBoostL2Tau_DRL2J1','p_DRll01','p_DRjl00','p_Mjl00','p_Mjl10','p_Mjl01','p_M_closerLepToTau','p_DR_closerJetToLeadLep','p_M_closerJetToLeadLep','p_DR_closerJetToSubLeadLep','p_invMl2j1j2','LBoost2L_AngleTauJ1','LBoost2L_AngleTauJ2','lead_jetPt','min_DR_lep_leadjet','min_M_lep_leadjet']




os.getcwd()
pwd = os.getcwd()
merge2 = '/'.join(pwd.split('/')[:-2])
# print(merge2)
# input_files = glob.glob(merge2)
input_files_med = glob.glob(merge2+'/merge2/Med*.root')
input_files_tight = list(set(glob.glob(merge2+'/merge2/*.root'))-set(glob.glob(merge2+'/merge2/Med*.root'))-set(glob.glob(merge2+'/merge2/Tight_new*.root')))
input_files_tight_new = glob.glob(merge2+'/merge2/Tight_new*.root')
all_files_dict = {}


for files in input_files_tight_new:
    print(files.split('/')[-1])
    name = files.split('/')[-1]
    all_files_dict[name] = pd.DataFrame.from_dict(uproot.open(files+':nominal').arrays(library="np"))

# all_files_pd = pd.DataFrame.from_dict(all_files_dict)
# Only keep some variables for training
# variable_names = ['lep_Pt_0','lep_Pt_1','HT_jets','Mll01','Ptll01','HT_lep','DRll01','lep_Phi_0','lep_Phi_1','lep_Eta_0','lep_Eta_1','tau_pt_0','nJets_OR','tau_eta_0','tau_phi_0','HT','met_met','met_phi','lead_jetPt','lead_jetEta','lead_jetPhi','sublead_jetPt','sublead_jetEta','sublead_jetPhi','angle_leadjet_tau','angle_subleadjet_tau','min_DR_lep_leadjet','min_DR_lep_subleadjet','min_DR_tau_jet','min_DR_tau_lep','min_M_lep_leadjet','min_M_lep_subleadjet','min_M_tau_jet','min_M_tau_lep','p_DRjl01','p_DRjl11','p_DRjl00','p_DRjl10','p_DRtauj00','p_DRtauj01','p_DRtaul01','p_DRtaul00','p_Mjl01','p_Mjl00','p_Mjl10','p_Mjl11','p_Mtaul01','p_Mtaul00','p_Mtauj01','p_Mtauj00','p_DRmetl0','p_DRmetl1','p_DRmettau0','p_DPhill01','p_Mmetl0','p_Mmetl1','p_Mmettau0','p_DR_close_jet_lead_led','p_DR_far_jet_lead_led','sum_pt','p_DPhijl00','p_Mjjl011']
# variable_names = ['p_Mjl01','min_DR_tau_lep','p_DR_close_jet_lead_led','min_M_tau_lep','p_DRll01','p_Mjl00','p_Mjjl011']

# v = ['lep_Pt_0','lep_Pt_1','HT_jets','Mll01','Ptll01','HT_lep','DRll01', ]
df_vv = all_files_dict["Tight_new_VV.root"]
df_sig1 = all_files_dict["Tight_new_signal_ggF.root"]
df_sig2 = all_files_dict["Tight_new_signal_VBF.root"]
df_sig = df_sig1.append(df_sig2)

df_sig['class'] = np.ones(len(df_sig[variable_names[0]]))
df_vv['class'] = np.zeros(len(df_vv[variable_names[0]]))

X = df_vv.append(df_sig)
X = X.sample(frac=1).reset_index(drop=True) # Sample  
# LBoostL1Tau_DRL1J2 = getpDeltaR(X['lep_Eta_0'],X['sublead_jetEta'],X['lep_Phi_0'],X['sublead_jetPhi'])
# LBoostL1Tau_DRL2J2 = getpDeltaR(X['lep_Eta_1'],X['sublead_jetEta'],X['lep_Phi_1'],X['sublead_jetPhi'])
weight = X.pop("w_lumi")
# X = ChangeScale(X,100,train_variable)
y = X.pop('class')
print(X[variable_names])
X=X[variable_names]
# X['LBoostL1Tau_DRL1J2'] = LBoostL1Tau_DRL1J2
# X['LBoostL1Tau_DRL2J2'] = LBoostL1Tau_DRL2J2

# assert False
# X = ChangeNorm(X,train_variable)
# weight = ChangeNormV(weight)
# y = y.replace(100,1)

# data = X
# label = pd.DataFrame(y)
# dtrain = xgb.DMatrix(data, label=label,weight=weight)
# print(dtrain)
# kfold = ps.split(X,y)
# print(kfold)
SEED = 1
N_FOLDS = 10
ps = StratifiedKFold(n_splits=N_FOLDS, shuffle=True, random_state=SEED)

CV_RESULT_DIR = os.getcwd()+"/xgboost_cv_results200-3/"
if not os.path.exists(CV_RESULT_DIR):  os.mkdir(CV_RESULT_DIR)

# import sklearn.datasets
# import xgboost as xgb
# ratio = float(np.sum(weight[y == 1]*y[y == 1])) / np.sum(weight[y==0]*y[y == 0])
# print(ratio)

# assert False
def objective(trial):


#     dtrain = xgb.DMatrix(X, label=pd.DataFrame(y),weight=weight)
#     ratio = float((np.sum(weight[y == 0]))/( np.sum(weight[y==1])))
#     print(ratio)


    param = {
        "verbosity": 0,
        "objective": "binary:logistic",
        "eval_metric": "logloss",
        "tree_method": "gpu_hist",
        "single_precision_histogram":True,
#         "booster": trial.suggest_categorical("booster", ["gbtree", "gblinear", "dart"]),
        "booster": trial.suggest_categorical("booster", ["gbtree", "dart"]),

        "lambda": trial.suggest_float("lambda", 1e-8, 1.0, log=True),
        "alpha": trial.suggest_float("alpha", 1e-8, 1.0, log=True),
    }

    if param["booster"] == "gbtree" or param["booster"] == "dart":
        param["max_depth"] = trial.suggest_int("max_depth", 2, 15)
        param["eta"] = trial.suggest_float("eta", 1e-8, 1.0, log=True)
        param["gamma"] = trial.suggest_float("gamma", 1e-8, 1.0, log=True)
#         param["max_depth"]=trial.suggest_int("max_depth", 2, 15)
        param["min_child_weight"]= trial.suggest_float("min_child_weight", 1e-8, 1.0, log=True)
        param["max_delta_step"]=trial.suggest_int("max_delta_step",0,10)
        param["subsample"]=trial.suggest_float("subsample", 0.1, 1.0, log=True)
        param["sampling_method"]=trial.suggest_categorical("sampling_method",["uniform","gradient_based"])
        param["colsample_bytree"]=trial.suggest_float("colsample_bytree", 0.1, 1.0, log=True)
        param["colsample_bylevel"]=trial.suggest_float("colsample_bylevel", 0.1, 1.0, log=True)
        param["colsample_bynode"]=trial.suggest_float("colsample_bynode", 0.1, 1.0, log=True)
        param["grow_policy"]=trial.suggest_categorical("grow_policy",['depthwise','lossguide'])
        
        
    if param["booster"] == "dart":
        param["sample_type"] = trial.suggest_categorical("sample_type", ["uniform", "weighted"])
        param["normalize_type"] = trial.suggest_categorical("normalize_type", ["tree", "forest"])
        param["rate_drop"] = trial.suggest_float("rate_drop", 1e-8, 1.0, log=True)
        param["skip_drop"] = trial.suggest_float("skip_drop", 1e-8, 1.0, log=True)
        
        
    ps = StratifiedKFold(n_splits=N_FOLDS, shuffle=True, random_state=SEED)
    kfold = ps.split(X,y)
#     ratio = float((np.sum(weight[y == 0]))/( np.sum(weight[y==1])))

    results_dict={}
    evals_result = {}
    
    c=0
    
    for train_index, test_index in ps.split(X,y):

        c +=1
        if c != FOLD: continue
    
        X_train, X_test = X.iloc[train_index], X.iloc[test_index]
        y_train, y_test = y.iloc[train_index], y.iloc[test_index]
        X_trainw, X_testw = weight.iloc[train_index],weight.iloc[test_index]

        dtrain = xgb.DMatrix(X_train, y_train,weight=X_trainw)
        dtest = xgb.DMatrix(X_test, y_test,weight=X_testw)


        ratio = float((np.sum(X_trainw[y_train == 0]))/( np.sum(X_trainw[y_train==1])))

        param[ "scale_pos_weight"]=ratio
#         print(f"\n best params for {c} Fold \n")
#         print(param)
        pruning_callback = optuna.integration.XGBoostPruningCallback(trial, "test-logloss")
#         num_boost_round = trial.suggest_int("num_boost_round",50,1000)
#         early_stopping_rounds = int(num_boost_round*0.1)
#         print("\n num_boost_round ",num_boost_round,"   early_stopping_rounds ",early_stopping_rounds)
        watchlist = [(dtest, 'test'), (dtrain, 'train')]
#         xgbc0 = xgb.train(param, dtrain,num_boost_round=2000,early_stopping_rounds=100,evals=watchlist,callbacks=[pruning_callback], verbose_eval=True,evals_result=evals_result)
        xgbc0 = xgb.train(param, dtrain,num_boost_round=200,early_stopping_rounds=20,evals=watchlist,callbacks=[pruning_callback], verbose_eval=True,evals_result=evals_result)
#     print(evals_result['test']['auc'])
#     print(pd.DataFrame.from_dict(evals_result)['test','train'])
#     assert False
    pd.DataFrame.from_dict({'test-logloss':evals_result['test']['logloss'],'train-logloss':evals_result['train']['logloss']}).to_csv(CV_RESULT_DIR+f'{trial.number}.csv')

    trial.set_user_attr("n_estimators", xgbc0.best_iteration)
    best_score = evals_result['test']['logloss'][-1]
    print('best_score',best_score)
    test_train_result = [abs(evals_result['test']['logloss'][-1]),abs(evals_result['train']['logloss'][-1])]
    delta_logloss = abs(max(test_train_result) - min(test_train_result))
    if delta_logloss > 0.02: 
        print(f' Trial Pruned delta metric  {delta_logloss}')
        raise optuna.TrialPruned()
    return best_score
    
FOLD=1
study_name = "study_weight2_fold1_hyper_200-3"  # Unique identifier of the study.
storage_name = "sqlite:///{}.db".format(study_name)
# pruner = optuna.pruners.MedianPruner(n_warmup_steps=20)
pruner = optuna.pruners.HyperbandPruner()
study = optuna.create_study(direction="minimize",pruner=pruner,storage=storage_name,study_name=study_name)
study.optimize(objective, n_trials=1000)

print("Best trial:")
trial = study.best_trial

print("  Value: {}".format(trial.value))

print("  Params: ")
for key, value in trial.params.items():
    print("    {}: {}".format(key, value))

print("  Number of estimators: {}".format(trial.user_attrs["n_estimators"]))



results_dict={}
c=0
for train_index, test_index in ps.split(X,y):
# for train_index, test_index in sk.split(X,y):
#     c +=1
#     print("TRAIN:", train_index, "TEST:", test_index)
    X_train, X_test = X.iloc[train_index], X.iloc[test_index]
    y_train, y_test = y.iloc[train_index], y.iloc[test_index]
    X_trainw, X_testw = weight.iloc[train_index],weight.iloc[test_index]
    
    dtrain = xgb.DMatrix(X_train, y_train,weight=X_trainw)
    dtest = xgb.DMatrix(X_test, y_test,weight=X_testw)


    ratio = float(np.sum(X_trainw[y_train == 0])) / np.sum(X_trainw[y_train==1])
    print("\n best params \n")
#     ratio = float((np.sum(weight[y == 0]))/( np.sum(weight[y==1])))

    params = study.best_params
#     params['num_boost_round']=study.best_trial.user_attrs
    params['objective']="binary:logistic"
    params["eval_metric"]="logloss"
    params["tree_method"]="gpu_hist"
    params[ "scale_pos_weight"]=ratio
    print(params)
    watchlist = [(dtest, 'eval'), (dtrain, 'train')]
    xgbc0 = xgb.train(params, dtrain,num_boost_round=study.best_trial.user_attrs['n_estimators'],evals=watchlist)
#     xgbc0 = xgb.train(params, dtrain,num_boost_round=study.best_trial.user_attrs['n_estimators'],evals=watchlist)

#     xgbc0 = xgb.train(param, dtrain,num_boost_round=500,early_stopping_rounds=50,evals=watchlist,callbacks=[pruning_callback], verbose_eval=True,evals_result=evals_result)
    
    print("XBDT VVVVVVVVVVVVVVVVVVVVVVVVV")
    print(xgbc0)
    results_dict[f'clf{c}_train_prob'] = xgbc0.predict(dtrain)
    results_dict[f'clf{c}_test_prob'] = xgbc0.predict(dtest)

    results_dict[f'y_test{c}'] = y_test
    results_dict[f'y_train{c}'] = y_train
    results_dict[f'clf{c}'] = xgbc0
    results_dict[f'X_train{c}'] = X_train
    results_dict[f'X_test{c}'] = X_test

    results_dict[f'{c}_test_weight'] = X_testw
#     results_dict[f'{c}_test_oldweight'] = oldweight.iloc[test_index]
#     results_dict[f'{c}_test_newweight'] = newWeight.iloc[test_index]
#     results_dict[f'{c}_test_weight_lumi_xsec'] = weight_lumi_xsec.iloc[test_index]
    plt.hist(xgbc0.predict(dtest)[y_test==0],label=f'{c}bkg test',density=True,alpha=0.5)
    plt.hist(xgbc0.predict(dtrain)[y_train==0],label=f'{c}bkg train',density=True,alpha=0.5,histtype='step')
    
    plt.hist(xgbc0.predict(dtest)[y_test==1],label=f'{c}sig',density=True,alpha=0.5)
    plt.hist(xgbc0.predict(dtrain)[y_train==1],label=f'{c}sig train',density=True,alpha=0.5,histtype='step')
    c=c+1
plt.legend()
plt.savefig(f'{study_name}_folds_test_train.png')
plt.close()
out1put_sig = np.concatenate([results_dict['clf{}_test_prob'.format(i)][ results_dict['y_test{}'.format(i)]==1] for i in range(N_FOLDS)],axis=0)
out1put_sigtrain = np.concatenate([results_dict['clf{}_train_prob'.format(i)][ results_dict['y_train{}'.format(i)]==1] for i in range(N_FOLDS)],axis=0)
out1put_bkg = np.concatenate([results_dict['clf{}_test_prob'.format(i)][ results_dict['y_test{}'.format(i)]==0] for i in range(N_FOLDS)],axis=0)
out1put_bkgtrain = np.concatenate([results_dict['clf{}_train_prob'.format(i)][ results_dict['y_train{}'.format(i)]==0] for i in range(N_FOLDS)],axis=0)

# out1put_sig_w = np.concatenate([results_dict[f'{i}_test_weight'][results_dict['y_test{}'.format(i)]==1] for i in range(N_FOLDS)],axis=0)
# out1put_bkg_w = np.concatenate([results_dict[f'{i}_test_weight'][results_dict['y_test{}'.format(i)]==0] for i in range(N_FOLDS)],axis=0)



# f = plt.figure()
plt.hist(out1put_sig,label='hh test',color='red',density=True,alpha=0.5)
plt.hist(out1put_sigtrain,label='hh train',color='red',density=True,alpha=0.5,histtype='step')
plt.hist(out1put_bkg,label='VV test',color='green',density=True,alpha=0.5)
plt.hist(out1put_bkgtrain,label='VV train',color='green',density=True,alpha=0.5,histtype='step')
plt.legend()
plt.savefig(f'{study_name}test_train.png')
# plt.close(f)
plt.close()
# f = plt.figure()
for i in range(N_FOLDS):
    
    roc1 = roc_curve(results_dict['y_test{}'.format(i)], results_dict['clf{}_test_prob'.format(i)])
    fpr,tpr,_=roc1
    plt.plot(fpr, tpr, 'b',label=f'{i} ROC (area = {auc(fpr,tpr):.2f})')
    plt.title(i)
    plt.legend()
#     plt.savefig(f'out/test1_gpu_{i}_roc.png')
plt.savefig(f'{study_name}roc.png')
# plt.close(f)

# plt.show()